# nodejs-ethereum-counter
A simple application to demonstrate the use of Block chain on Ethereum network to deploy a Smart contract with a increment and decrement operation.

A blockchain is a public database that is updated and shared across many computers in a network. "Block" refers to data and state being stored in consecutive groups known as "blocks". "Chain" refers to the fact that each block cryptographically references its parent. In other words, blocks get chained together. The data in a block cannot change without changing all subsequent blocks, which would require the consensus of the entire network.

### What is Ethereum
In the Ethereum universe, there is a single, canonical computer (called the Ethereum Virtual Machine, or EVM) whose state everyone on the Ethereum network agrees on. Everyone who participates in the Ethereum network (every Ethereum node) keeps a copy of the state of this computer. Additionally, any participant can broadcast a request for this computer to perform arbitrary computation. Whenever such a request is broadcast, other participants on the network verify, validate, and carry out ("execute") the computation. This execution causes a state change in the EVM, which is committed and propagated throughout the entire network.

### What is Ether
Ether (ETH) is the native cryptocurrency of Ethereum. The purpose of ether is to allow for a market for computation.

### What is a Smart contract
A "smart contract" is simply a program that runs on the Ethereum blockchain. It's a collection of code (its functions) and data (its state) that resides at a specific address on the Ethereum blockchain.

### What are Dapps
A decentralized application (dapp) is an application built on a decentralized network that combines a smart contract and a frontend user interface.

## Language Used
1. NodeJS
2. Solidity: Solidity is an object-oriented, high-level language for implementing smart contracts.

## Tools Used
1. Ganache: Ganache is a personal blockchain for rapid Ethereum distributed application development (Dapps).
2. Truffle Suite: Truffle Suit is a development environment, testing framework and asset pipeline for blockchains using the Ethereum Virtual Machine (EVM). 
3. Metamask: MetaMask is an extension for accessing Ethereum enabled distributed applications, or "Dapps" in your browser!

## Commands to install Project
```javascript
npm install -g truffle@5.0.2
truffle init
npm init
npm install
```
## Commands to compile and deploy the Smart Contract to the local Blockchain
```javascript
truffle compile
truffle migrate
```
## Commands check the Smart Contract deployed in the Blockchain
```javascript
truffle console
```

## Truffle console commands
```javascript
counterObj = await Counter.deployed()
value = await counterObj.get()
value.toNumber()

counterObj.inc()
value = await counterObj.get()
value.toNumber()

counterObj.dec()
value = await counterObj.get()
value.toNumber()

.exit()
```
